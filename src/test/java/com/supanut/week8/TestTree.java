package com.supanut.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestTree {
    @Test
    public void sholdCreateTreeSuccess1() {
        Tree Tree = new Tree("Tree", 'T', 5,10);
        assertEquals("Tree", Tree.getName());
        assertEquals('T', Tree.getSymbol());
        assertEquals(5, Tree.getX());
        assertEquals(10, Tree.getY());
    }

    @Test
    public void sholdCreateTreeSuccess2() {
        Tree Tree = new Tree("Tree", 'T' ,5 ,11);
        assertEquals("Tree", Tree.getName());
        assertEquals('T', Tree.getSymbol());
        assertEquals(5, Tree.getX());
        assertEquals(11, Tree.getY());
    }

    @Test
    public void sholdCreateTreeMakPai() {
        Tree Tree = new Tree("Tree", 'T', 200,30);
        assertEquals("Tree", Tree.getName());
        assertEquals('T', Tree.getSymbol());
        assertEquals(19, Tree.Xmakpai());
        assertEquals(19, Tree.Ymakpai());
    }
    
    @Test
    public void sholdCreateTree0() {
        Tree Tree = new Tree("Tree", 'T', -5,-8);
        assertEquals("Tree", Tree.getName());
        assertEquals('T', Tree.getSymbol());
        assertEquals(0, Tree.X0());
        assertEquals(0, Tree.Y0());
    }
}
