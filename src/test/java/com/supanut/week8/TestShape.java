package com.supanut.week8;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestShape {

    @Test
    public void sholdFindArea1Success1() {
        Shape rect = new Shape("rect", 2, 3);
        assertEquals(6, rect.FindArea1(),0.1);;
    }
    @Test
    public void sholdFindArea1Success2() {
        Shape rect = new Shape("rect", 1, 3);
        assertEquals(3, rect.FindArea1(),0.1);;
    }

    @Test
    public void sholdFindArea2Success1() {
        Shape O = new Shape("O", 5);
        assertEquals(78.5, O.FindArea2(),0.1);;
    }
    @Test
    public void sholdFindArea2Success2() {
        Shape O = new Shape("O", 10);
        assertEquals(314.2, O.FindArea2(),0.1);;
    }
    @Test
    public void sholdFindArea3Success() {
        Shape Tri = new Shape("Tri", 3, 3, 3);
        assertEquals(3.9, Tri.FindArea3(),0.1);;
    }

    @Test
    public void sholdFindPerimeter1Success() {
        Shape rect = new Shape("rect", 2, 2);
        assertEquals(8, rect.FindPerimeter1(),0.1);;
    }
    @Test
    public void sholdFindPerimeter2Success() {
        Shape O = new Shape("O", 2);
        assertEquals(12.6, O.FindPerimeter2(),0.1);;
    }
    @Test
    public void sholdFindPerimeter3Success() {
        Shape Tri = new Shape("Tri", 3, 3, 3);
        assertEquals(9, Tri.FindPerimeter3(),0.1);;
    }

}
