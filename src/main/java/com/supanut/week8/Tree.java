package com.supanut.week8;

public class Tree {
    private String name;
    private char symbol;
    private int x ;
    private int y ;
    public static final int MIN_X = 0;
    public static final int MIN_Y = 0;
    public static final int MAX_X = 19;
    public static final int MAX_Y = 19;

    public Tree(String name,char symbol, int x,int y) {
        this.name = name;
        this.symbol = symbol;
        this.x = x;
        this.y = y;
    }
    public int Xmakpai() {
        if(x>19) {
            return x=19;
        } else {
            return x;
        }
    }
    public int Ymakpai() {
        if(y>19) {
            return y=19;
        } else {
            return y;
        }
    }
    public int X0() {
        if(x<0) {
            return x=0;
        } else {
            return x;
        }
    }
    public int Y0() {
        if(y<0) {
            return y=0;
        } else {
            return y;
        }
    }

    public Tree(String name,char symbol) {
        this(name, symbol, 0, 0);
    }
    public void print() {
        System.out.println("Tree: " + name + " X:" + x + " Y:" + y);
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public char getSymbol() {
        return symbol;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}

