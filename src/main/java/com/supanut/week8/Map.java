package com.supanut.week8;

public class Map {
    private int width ;
    private int height ;
    public Map(int width ,int height) {
        this.width = width;
        this.height = height;
    }
    public int getWidth() {
        return width;
    }
    public void setWidth(int w) {
        this.width = w;
    }

    public int getHeight() {
        return height;
    }
    public void setHeight(int h) {
        this.height = h;
    }

    public void print() {
    }
    
}
