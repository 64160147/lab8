package com.supanut.week8;

public class Shape {
    private String name;
    private float width;
    private float height;
    public Shape(String name ,float width ,float height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }
    public void print1() {
        System.out.println(name + " / Width= " + width + " / Height= " + height);
    }
    public float FindArea1() {
        float area;
        area = width * height;
        System.out.println("Area= "+area);
        return area;
    }
    public float FindPerimeter1() {
        float Perimeter;
        Perimeter = (width*2) + (height*2);
        System.out.println("Perimeter= "+Perimeter);
        return Perimeter;
    }

    private float r;
    public Shape(String name , float r) {
        this.name = name;
        this.r = r;
    }
    public void print2() {
        System.out.println(name + " / Radius= " + r );
    }
    public float FindArea2() {
        float area;
        area = (float)(Math.PI * (r*r) );
        System.out.println("Area= "+area);
        return area;
    }
    public float FindPerimeter2() {
        float Perimeter;
        Perimeter = (float)(2*(Math.PI)*r);
        System.out.println("Perimeter= "+Perimeter);
        return Perimeter;
    }

    private float a;
    private float b;
    private float c;
    public Shape(String name ,float a ,float b ,float c) {
        this.name = name;
        this.a = a;
        this.b = b;
        this.c = c;
    }
    public void print3() {
        System.out.println(name + " / a= " + a + " / b= " + b + " / c= " + c);
    }
    public float FindArea3() {
        float area;
        float s = (a + b + c)/2;
        area = (float)(Math.sqrt(s*(s-a)*(s-b)*(s-c)));
        System.out.println("Area= "+area);
        return area;
    }
    public float FindPerimeter3() {
        float Perimeter;
        Perimeter = a + b + c;
        System.out.println("Perimeter= "+Perimeter);
        return Perimeter;
    }
    
}
