package com.supanut.week8;

public class ShapeApp {
    public static void main(String[] args)
     {
        Shape rect1 = new Shape("rect1", 10, 5);
        rect1.print1();
        rect1.FindArea1();
        rect1.FindPerimeter1();
        
        Shape rect2 = new Shape("rect2",5,3);
        rect2.print1();
        rect2.FindArea1();
        rect2.FindPerimeter1();

        Shape circle1 = new Shape("circle1", 1) ;
        circle1.print2();
        circle1.FindArea2();
        circle1.FindPerimeter2();

        Shape circle2 = new Shape("circle1", 2) ;
        circle2.print2();
        circle2.FindArea2();
        circle2.FindPerimeter2();

        Shape triangle = new Shape("triangle", 5, 5, 6) ;
        triangle.print3();
        triangle.FindArea3();
        triangle.FindPerimeter3();
    }
}
    
